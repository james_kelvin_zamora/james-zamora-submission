require("dotenv").config();
const express = require("express");
const cors = require("cors");
const pgClient = require("./db");
const uuid = require('uuid/v4');

//pgClient.on('error', () => console.log('Lost Postgres connection'));

// TODO: Create initial DB table called task
pgClient
  .query(
    "CREATE TABLE IF NOT EXISTS task ("+
      "id UUID NOT NULL,"+
      "title TEXT,"+
      "details TEXT,"+
      "completed BOOLEAN DEFAULT false,"+
      "PRIMARY KEY (id)"+
    ");"+
    "TRUNCATE task;"
  )
  .catch((err) => console.log(err));

const app = express();

app.use(cors());
app.use(express.json());

// Get all to do list tasks
app.get('/api/v1/tasks', async (req, res) => {
  //TODO: Insert your route logic here
  try {
    //const results = await pgClient.query("select * from task");
    const todoList = await pgClient.query(
      "SELECT * FROM task;"
    );

    res.status(200).json({
      status: "success",
      results: todoList.rows.length,
      data: {
        tasks: todoList.rows,
      },
    });
  } catch (err) {
    console.log(err);
  }
});

//Get a single todo task
app.get('/api/v1/tasks/:id', async (req, res) => {
  // TODO: Insert your route logic here
  console.log(req.params.id);

  try {
    const todo = await pgClient.query(
      "SELECT * FROM task WHERE id = $1;",
      [req.params.id]
    );

    res.status(200).json({
      status: "success",
      data: {
        task: todo.rows[0],
      },
    });
  } catch (err) {
    console.log(err);
  }
});

// // Create a todo task
app.post('/api/v1/tasks', async (req, res) => {
  // TODO: Insert your route logic here
  try {
    const id = uuid();
    const results = await pgClient.query(
      "INSERT INTO task (id, title, details, completed) VALUES($1, $2, $3, $4) returning *",
      [id, req.body.title, req.body.details, false]
    );
    console.log(results.rows[0]);
    res.status(201).json({
      status: "success",
      data: {
        task: results.rows[0],
      },
    });
  } catch (err) {
    console.log(err);
  }
});

app.put("/api/v1/tasks/:id/:toggle", async (req, res) => {
  try {

    if(req.params.toggle){
        console.log("Toggle: ", req.params.toggle);
    }
    const tod = await pgClient.query(
      "SELECT * FROM task WHERE id = $1;",
      [req.params.id]
    );
    var coms = false;
    if(tod.rows[0].completed){
      coms = false;
    } else {
      coms = true;
    }
 
    console.log("Toggle", coms);
    const toggleCompleted = await pgClient.query(
      "UPDATE task SET completed = $1 where id = $2 returning *",
      [coms, req.params.id]
    );
    console.log(toggleCompleted.rows[0]);
    res.status(200).json({
      status: "success",
      data: {
        task: toggleCompleted.rows[0],
      },
      toggle: true,
    });
  } catch (err) {
    console.log(err);
  }
});

// // Update a todo task
app.put('/api/v1/tasks/:id', async (req, res) => {
  // TODO: Insert your route logic here
  try {
    
    console.log("checkBody",req.params);
    const results = await pgClient.query(
      "UPDATE task SET title = $2, details = $3 where id = $1 returning *",
      [req.params.id, req.body.title, req.body.details]
    );
    console.log("Updated: ", results.rows[0]);
    res.status(200).json({
      status: "success",
      data: {
        task: results.rows[0],
      },
    });
    
  } catch (err) {
    console.log(err);
  }
  //console.log(req.params.id);
  //console.log(req.body);
});



// // Delete a todo task route

app.delete('/api/v1/tasks/:id', async (req, res) => {
  // TODO: Insert your route logic here
  try {
    const results = pgClient.query("DELETE FROM task where id = $1", [
      req.params.id,
    ]);
    res.status(204).json({
      status: "success",
    });
  } catch (err) {
    console.log(err);
  }
});

// app.put("/api/v1/task/:id", async (req, res) => {
//   try {
//     console.log("Toggle", req.params);
//     const tod = await pgClient.query(
//       "SELECT * FROM task WHERE id = $1;",
//       [req.params.id]
//     );
//     var coms = false;
//     if(tod.rows[0].completed){
//       coms = false;
//     } else {
//       coms = true;
//     }
 
//     console.log("Toggle", coms);
//     const toggleCompleted = await pgClient.query(
//       "UPDATE task SET completed = $1 where id = $2 returning *",
//       [coms, req.params.id]
//     );
//     console.log(toggleCompleted.rows[0]);
//     res.status(200).json({
//       status: "success",
//       data: {
//         task: toggleCompleted.rows[0],
//       },
//       toggle: true,
//     });
//   } catch (err) {
//     console.log(err);
//   }
// });

const port = process.env.PORT || 3001;
app.listen(port, () => {
  console.log(`server is up and listening on port ${port}`);
});
