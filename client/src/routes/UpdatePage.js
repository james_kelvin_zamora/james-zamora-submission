import React from "react";
import UpdateTask from "../components/UpdateTask";

const UpdatePage = () => {
  return (
    <div>
      <h1 className="text-center">Update Task</h1>
      <UpdateTask />
    </div>
  );
};

export default UpdatePage;
